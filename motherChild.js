// Read ancestry file
var module = require("./ancestry.js");
var ancestry = JSON.parse(module);

function average(array) {
  function plus(a, b) { return a + b; }
  return array.reduce(plus) / array.length;
}

var byName = {};
ancestry.forEach(function(person) {
  byName[person.name] = person;
});

var ageDifferences = [];
for (let name in byName) {
	let e = byName[name];
	if (e.born != null && e.mother != null && byName[e.mother] != null && byName[e.mother].born != null) {
		ageDifferences.push(Math.abs(e.born - byName[e.mother].born));
		continue;
	}
	continue;
}

console.log(average(ageDifferences));