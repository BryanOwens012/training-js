function arrayToList(arr) {
	var newList = null;
	for (let i = arr.length - 1; i >= 0; i--) {
		newList = {value: arr[i], rest: newList};
	}
	return newList;
}

function listToArray(list) {
	var arr = [];
	// for each node in the list, until node != true (i.e., node == null);
	for (let node = list; node; node = node.rest) {
		arr.push(node.value);
	}
	return arr;
}
function prepend(e, list) {
	var newList = {value: e, rest: list};
	return newList;
}

function nth(list, num) {
	var node = list;
	for (let i = 0; i < num; i++) {
		node = node.rest;
	}
	return node.value;
}

console.log(arrayToList([1, 2, 3]));
console.log(listToArray({value: 1, rest: {value: 2, rest: {value: 3, rest: null}}}));
console.log(prepend("asdf", {value: 1, rest: {value: 2, rest: null}}));
console.log(nth({value: 1, rest: {value: 2, rest: null}}, 0));