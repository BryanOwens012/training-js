function min(a, b) {
	// Return a if a <= b, otherwise return b
	return (a <= b) ? a : b;
}

// Testing
var a = 0, b = -10
console.log(min(a, b));