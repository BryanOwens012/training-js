var size = 9;
var str = "";

for (var i = 1; i <= Math.ceil(size / 2) * 2 - 1; i++) {
	// Generate the string composed of # that can be reused for each row, offset with a space if necessary
	// E.g., size = 8 --> "# # # #"; size 9 --> "# # # # #"
	str += (i % 2 == 1) ? "#" : " ";
}

for (var i = 0; i < size; i++) {
	if (size % 2 == 0) {
		/* If size is even, then the board needs to look something like this:
		# # # #
		 # # # #
		# # # #
		 # # # #
		*/
		console.log((i % 2 == 0) ? (str + " ") : (" " + str));
	}
	else if (size % 2 == 1) {
		/*
		If size is odd, then it should look like (note the differing number of # on alternating lines):
		# # # # #
		 # # # #
		# # # # #
		 # # # #
		*/
		console.log((i % 2 == 0)? str : (" " + str.substring(2, str.length) + " "));
	}
}