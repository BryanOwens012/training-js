function deepEqual(val1, val2) {
	if (val1 == null && val2 == null) {return true;}
	if ((val1 == null && val2 != null) || (val1 != null && val2 == null)) {
		return false;
		}
	if (typeof val1 != "object" && typeof val2 != "object") {
		return val1 === val2 ? true : false;
	}
	if (typeof val1 == "object" && typeof val2 == "object") {
		var keyList1 = Object.keys(val1);
		var keyList2 = Object.keys(val2);
		if (keyList1.length != keyList2.length) {return false};
		for (let i = 0; i < keyList1.length; i++) {
			if (keyList1[i] != keyList2[i]) {return false};
			return (deepEqual(val1.keyList1[i], val2.keyList2[i]));
		}
	}
	return true;
}

// Testing

var obj = {here: {is: "an"}, object: 2};
console.log(deepEqual(obj, obj));
// → true
console.log(deepEqual(obj, {here: 1, object: 2}));
// → false
console.log(deepEqual(obj, {here: {is: "an"}, object: 2}));
// → true