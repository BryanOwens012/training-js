// For/in changes NaN to 0 for some reason, so we have to use a normal for loop
function every(arr, condition) {
	for (let i = 0; i < arr.length; i++) {
		if (!condition(arr[i])) {return false;}
	}
	return true;
}

function some(arr, condition) {
	for (let i = 0; i < arr.length; i++) {
		if (condition(arr[i])) {return true;}
	}
	return false;
}


/**** Seems that this version can't deal with NaNs
function every(arr, condition) {
	if (arr.forEach(function(el) {
		if (!condition(el)) {return false;}
	}) == false) {
		return false;
	}
	return true;
}

function some(arr, condition) {
	if (arr.forEach(function(el) {
		if (condition(el)) {return true;}
	}) == true) {
		return true;
	}
	return false;
}
*/