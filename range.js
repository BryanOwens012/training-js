function range(start, end) {
	var arr = [];
	for (var i = start; i <= end; i++) {
		arr.push(i);
	}
	return arr;
}

function range_step(start, end, step) {
	var arr = [];
	if (step == undefined) {step = 1};
	if (step == 0 && start == end) {return [start];}
	if (step == 0 && start != end) {return "Invalid step";}
	if (step < 0) {
		if (start < end) {return "Invalid step"};
		for (let i = start; i >= end; i += step) {
		arr.push(i);
		}
		return arr;
	}
	// Now we can assume that step > 0
	if (start > end) {return "Invalid step"};
	for (let i = start; i <= end; i += step) {
		arr.push(i);
	}
	return arr;
}

function sum(arr) {
	var sum = 0;
	for (let i = 0; i < arr.length; i++) {
		sum += arr[i];
	}
	return sum;
}

// Testing
console.log(range_step(1, 10));
console.log(range_step(10, 2, 2));

console.log(sum([0, 1, 2, 3]));