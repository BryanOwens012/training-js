function isEven(number) {
	// Corresponding to the given description of number parity (even/odd)
	if (number < 0 || isNaN(number)) return false;
	else if (number == 0) return true;
	else if (number == 1) return false;
	else return isEven(number - 2);
}

// Testing the function
console.log(isEven(50));
console.log(isEven(75));
console.log(isEven(-1));
console.log(isEven("asdfasdfasdf"));