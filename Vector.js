function Vector(x, y) {
	this.x = x;
	this.y = y;
	Object.defineProperty(this, "length", {
		get: function() {
			return Math.sqrt(this.x**2 + this.y**2);
	}}); // no setter necessary
}

Vector.prototype.plus = function(vect) {
	return new Vector(this.x + vect.x, this.y + vect.y);
}
Vector.prototype.minus = function(vect) {
	return new Vector(this.x - vect.x, this.y - vect.y);
}

// defineProperty: https://stackoverflow.com/questions/18524652/how-to-use-javascript-object-defineproperty
// Getters and setters: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/defineProperty

/* This way is correct plus() and minus() but leaves them as properties inside the object
function Vector(x, y) {
	this.x = x;
	this.y = y;
	this.plus = function(vect) {
		return (new Vector(this.x + vect.x, this.y + vect.y));
	}
	this.minus = function(vect) {
		return (new Vector(this.x - vect.x, this.y - vect.y));
	}
}
*/

// Testing
console.log(new Vector(1, 2).plus(new Vector(2, 3)));
// → Vector{x: 3, y: 5}
console.log(new Vector(1, 2).minus(new Vector(2, 3)));
// → Vector{x: -1, y: -1}
console.log(new Vector(3, 4).length);
// → 5