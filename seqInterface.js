function logFive(inner) {
	this.length = inner.length();
	for (let i = 0; i < Math.min(this.length, 5); i++) {
		console.log(inner.getElement(i));
	}
}

function ArraySeq(arr) {
	this.arr = arr;
	ArraySeq.prototype.length = function() {
		return this.arr.length;
	}
	ArraySeq.prototype.getElement = function(index) {
		return this.arr[index];
	}
}

function RangeSeq(from, to) {
	this.from = from;
	this.to = to;
	RangeSeq.prototype.length = function() {
		return this.to - this.from + 1;
	}
	RangeSeq.prototype.getElement = function(index) {
		return from + index;
	}
}

// Testing
logFive(new ArraySeq([1, 2]));
// → 1
// → 2
logFive(new RangeSeq(100, 1000));
// → 100
// → 101
// → 102
// → 103
// → 104