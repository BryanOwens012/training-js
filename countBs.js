function countBs(str) {
	var count = 0;
	for (var i = 0; i < str.length; i++) {
		if (str.charAt(i) == "B") {count++;}
	}
	return count;
}

function countChar(str, chr) {
	var count = 0;
	for (var i = 0; i < str.length; i++) {
		if (str.charAt(i) == chr) {count++;}
	}
	return count;
}

console.log(countChar("kakkerlak", "k"));