// Link: http://eloquentjavascript.net/05_higher_order.html#h_FkNn96IrQe

// Read ancestry file
var module = require("./ancestry.js");
var ancestry = JSON.parse(module);

// Given
function average(array) {
  function plus(a, b) { return a + b; }
  return array.reduce(plus) / array.length;
}

// To facilitate datum extraction
var byName = {};
ancestry.forEach(function(person) {
  byName[person.name] = person;
});

function toCentury(deathYear) {
	return Math.ceil(deathYear / 100);
}

// Find the earliest century
var minCentury = toCentury(ancestry.reduce(function(min, curr) {
	if (curr.died == null) {return min;}
	if (curr.died < min.died) {return curr;}
	else return min;
}).died);
var maxCentury = 21;

// Place people's lifespans into centuries
var centuryLifeSpans = {};
for (let i = minCentury; i <= maxCentury; i++) {
	centuryLifeSpans[i] = ancestry.filter(function(person) {
		return toCentury(person.died) == i;
	}).map(function(person) {
		return person.died - person.born;
	})
}

// Find the averages:
var centuryAverages = {};
for (let i = minCentury; i <= maxCentury; i++) {
	centuryAverages[i] = average(centuryLifeSpans[i]);
}

console.log(centuryAverages);
