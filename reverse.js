function reverseArray(arr) {
	var newArr = [];
	for (let i = 0; i < arr.length; i++) {
		newArr.unshift(arr[i]);
	}
	return newArr;
}

function reverseArrayInPlace(arr) {
	for (let i = 0; i < Math.floor(arr.length/2); i++) {
		let temp = arr[i];
		arr[i] = arr[arr.length - i - 1];
		arr[arr.length - i - 1] = temp;
	}
	return;
}

var arr1 = [1, 2, 3, 4];
console.log(reverseArray(arr1));

var arr2 = [2, 3, 4, 5, 6];
reverseArrayInPlace(arr2);
console.log(arr2);